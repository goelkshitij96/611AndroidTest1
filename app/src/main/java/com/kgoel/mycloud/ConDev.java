package com.kgoel.mycloud;

public class ConDev {

    private final String device;
    private final boolean available;

    public ConDev(String device, boolean available) {
        this.device = device;
        this.available = available;
    }

}
