package com.kgoel.mycloud;

import android.app.Activity;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.kgoel.mycloud.Implementation.RPi1;
import com.kgoel.mycloud.Implementation.RPi1Impl;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNReconnectionPolicy;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Objects;


public class MainActivityProvider extends ContentProvider {

//    static final int SERVER_PORT = 10000;
    static final String subscriberKey = "sub-c-b3f1894c-b61b-11e8-9c8c-5aa277adf39c";
    static final String publisherKey = "pub-c-a48eea9b-bec6-437f-a198-c629b1d05c4c";
    static final String subscribeChannel = "Hub Channel";
    static final String publishChannel = "Mobile Channel";

    private PubNub pubNubInitialisation() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(subscriberKey);
        pnConfiguration.setPublishKey(publisherKey);
//        pnConfiguration.setSecure(true);
        pnConfiguration.setReconnectionPolicy(PNReconnectionPolicy.LINEAR);
        PubNub pub = new PubNub(pnConfiguration);
        pub.grant().channels(Arrays.asList(publishChannel,subscribeChannel)).authKeys(Arrays.asList(publisherKey,subscriberKey)).ttl(5).read(true).write(true);
        return pub;
    }

    private void pubNubPublish(PubNub pubNub, TransmitObject obj){
        JSONObject jsonObject = obj.toJSON();
        pubNub.publish().message(jsonObject).channel(publishChannel)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        // handle publish result, status always present, result if successful
                        // status.isError() to see if error happened
                        if(!status.isError()) {
//                            System.out.println("pub timetoken: " + result.getTimetoken());
                            Log.d("kshitij","Publish success at time:" + result.getTimetoken());
                        }
                        else {
                            Log.d("kshitij", "Publish fail with code:" + status.getStatusCode());

//                        System.out.println("pub status code: " + status.getStatusCode());
                        }
                    }
                });
    }

    private void pubNubSubscribe(PubNub pubNub){
        pubNub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
                Activity activity = new Activity();
                TransmitObject transmitObject = new TransmitObject();
                Log.d("kshitij","Listener received message: "+ transmitObject.deviceType);
                transmitObject.deviceType = String.valueOf(message.getMessage().getAsJsonObject().get("deviceType"));
                Log.d("kshitij","Listener received message: "+ transmitObject.message);
                transmitObject.message = String.valueOf(message.getMessage().getAsJsonObject().get("message"));
                PassClass passClass = new PassClass();
                passClass.transmitObject = transmitObject;
                passClass.pubNub = pubnub;
                passClass.activity = activity;
                new ServerTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, passClass);
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });
        pubNub.subscribe().channels(Arrays.asList(publishChannel)).execute();
    }

    @Override
    public boolean onCreate() {
        Activity activity = new Activity();
        View R = activity.getCurrentFocus();

        Log.d("kshitij","Entering pi1 provider");
        PubNub pubNub = pubNubInitialisation();
        Log.d("kshitij","After pubnub initialisation");

        pubNubSubscribe(pubNub);
        Log.d("kshitij","After pubnub addListener");
        Log.d("kshitij","After pubnub subscribe");



        String testSend = "Test send from android app to ClientTask";
        PassClass passClass = new PassClass();
        passClass.transmitObject.message = testSend;
        passClass.pubNub = pubNub;
        passClass.activity = activity;
        new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, passClass);


        return false;
    }

    private class ServerTask extends AsyncTask<PassClass, Void, Void>{

        @Override
        protected Void doInBackground(PassClass... passClasses) {
            TransmitObject transmitObject = passClasses[0].transmitObject;
            PubNub pubNub = passClasses[0].pubNub;
            Activity activity = passClasses[0].activity;
            if(transmitObject.deviceType.compareTo("RPi1")==0 && activity.getCallingActivity().toString().compareTo("0")==0){
                try {
                    RPi1Impl rPi1Impl = (RPi1Impl) Class.forName(transmitObject.deviceType+"Impl").newInstance();
                    rPi1Impl.updateUI(activity, transmitObject.message);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            else if(transmitObject.deviceType.compareTo("RPi2")==0){

            }
            try {
                RPi1 rPi1 = (RPi1) Class.forName(transmitObject.deviceType).newInstance();

            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
//            transmitObject.deviceType;
            Log.d("kshitij","After pubnub publish");
            return null;
        }

    }

    private class ClientTask extends AsyncTask<PassClass, String, Void>{

        @Override
        protected Void doInBackground(PassClass... passClasses) {
            String msgToSend = passClasses[0].transmitObject.message;
            PubNub pubNub = passClasses[0].pubNub;
            Log.d("kshitij","ClientTask msg to send: " + msgToSend);
            for(int i=0;i<5;i++){
                String msg="Testing "+i;
                Log.d("kshitij","Publishing: "+ msg);
                TransmitObject transmitObject = new TransmitObject();
                transmitObject.deviceType=msg;
                pubNubPublish(pubNub, transmitObject);
                Log.d("kshitij","After pubnub publish iteration: " + i);
            }
            return null;
        }
    }

    private class PassClass {
        public PubNub pubNub;
        public TransmitObject transmitObject;
        public Activity activity;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
