package com.kgoel.mycloud;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ConnectedDevices extends BaseAdapter{

    private final Context mContext;
    private final String[] devices;
//    private ConDev[] conDevs;

//    public ConnectedDevices(Context context, Integer size) {
//        ConDev[] conDev = new ConDev[size];
//        conDev[0]= new ConDev("Raspberry Pi 1", true);
////        this.listOfDevices.add(conDev);
//        this.mContext = context;
//        this.conDevs = conDev;
//    }

    public ConnectedDevices(Context context, String device) {
//        ConDev[] conDev = new ConDev[size];
//        conDev[0]= new ConDev("Raspberry Pi 1", true);
//        this.listOfDevices.add(conDev);
        String[] dev = new String[1];
        dev[0]=device;
        this.mContext = context;
        this.devices = dev;
    }

    @Override
    public int getCount() {
        return devices.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView dummyTextView = new TextView(mContext);
        for( int i=0;i<=position;i++){
            dummyTextView.setText(this.devices[i]);
            dummyTextView.setTextSize(20);
        }
        return dummyTextView;
//        dummyTextView.setText(String.valueOf(position));
    }
}
