package com.kgoel.mycloud.Activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.kgoel.mycloud.R;

public class RaspberryPi1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raspberry_pi1);
        TextView textView = findViewById(R.id.textView1);
        textView.setText("test");
        final Uri uri= buildUri("content","com.kgoel.mycloud.provider");

//        getContentResolver().query(uri);
    }

    private Uri buildUri(String scheme, String authority) {
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.authority(authority);
        uriBuilder.scheme(scheme);
        return uriBuilder.build();
    }
}
