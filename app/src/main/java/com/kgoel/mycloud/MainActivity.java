package com.kgoel.mycloud;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ContentProvider contentProvider = new ContentProvider();
        Button RaspberryPi1 = findViewById(R.id.button1);
        RaspberryPi1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent deviceIntent = new Intent(getApplicationContext(), com.kgoel.mycloud.Activity.RaspberryPi1.class);
                startActivity(deviceIntent);
            }
        });

//        TextView textView = findViewById(R.id.textView1);
//        GridView gridview = (GridView) findViewById(R.id.gridview);
//        gridview.setAdapter(new ImageAdapter(this));
        ConnectedDevices connectedDevices = new ConnectedDevices(this,"Raspberry Pi 1");
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        Cursor cur = new Cursor(new String[]{"Device","Available"});
//        matCur.addRow(new String[]{"Raspberry Pi 1", "Yes"});
//        Log.d("kshitij","Showing matCur Size: "+ matCur.getCount());
//        ListAdapter listAdapter = (ListAdapter) matCur;
//        Log.d("kshitij","Showing listAdapter Size: "+ listAdapter.getCount());
        Log.d("kshitij","connected devices count: " + connectedDevices.getCount());
//        gridview.setAdapter(connectedDevices);
//        Log.d("kshitij","GridView count: " + gridview.getCount());

//        textView.setText("kshitij");
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v,
//                                    int position, long id) {
//                Toast.makeText(MainActivity.this, "" + position,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });





//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        final Button red = findViewById(R.id.toggleButton1);
//        final Button yellow = findViewById(R.id.toggleButton2);
//        final Button green = findViewById(R.id.toggleButton3);
//
//        red.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//
//        yellow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//
//        green.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
    }
}
